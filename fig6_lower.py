"""Code to generate figure 6 lower panel."""

import numpy as np
import matplotlib.pyplot as plt

from utils import adapt_save_fig, get_graphical_options
from matplotlib.lines import Line2D

plt.rcParams.update({'font.size': 30,
                     'legend.fancybox': True})
options = get_graphical_options()
colors = options['colors_genes']
genes = options['names_genes'][:-1]
names_bio = options['names_biomarkers']
col_bio = options['colors_biomarkers']
feats = names_bio + ['$\int$' + f for f in names_bio]
ngenes, nfeats = len(genes), len(feats)

# repartition of regulators in selected best models
bmal_hist = np.array([1, 1, 0, 1, 1, 2, 4, 2, 7, 1])
per_hist = np.array([0, 0, 0, 2, 0, 1, 1, 1, 1, 0])
hist = [bmal_hist, per_hist]
nmodels = np.array(np.sum(hist, axis=1) / 2, dtype=np.int32)
models = '{models}'
fig, axes = plt.subplots(1, 2, figsize=(25, 2))
for g in range(ngenes):
    idx_sorted = np.argsort(hist[g])[::-1]
    plot = idx_sorted[:-1] if not g else idx_sorted[:5]
    for l in range(len(plot)):
        axes[g].bar(l, hist[g][plot[l]],
                    color=col_bio[plot[l] % 5], width=.4)

    axes[g].set_xticks(np.arange(len(plot)))
    axes[g].set_xticklabels([feats[p] for p in plot], rotation=90)
    axes[g].set_title(f'${genes[g]}: n_{models}={nmodels[g]}$')

lines = [Line2D([0], [0], color='w', markerfacecolor=c,
         marker='s', markersize=15) for c in col_bio] + \
        [Line2D([0], [0], color='w', markerfacecolor='black',
         marker='s', markersize=15)] + \
        [Line2D([0], [0], color='w', markeredgecolor='black',
         marker='s', markersize=15, linestyle='--')]

labels = ['Biomarker', '$\int$ Biomarker']
fig.legend(lines, names_bio + labels,
           bbox_to_anchor=(1.1, 1.4), shadow=True, ncol=1)
fig.text(.1, 1.075, r'$\bf{B}$')
fig.text(.52, 1.075, r'$\bf{C}$')
adapt_save_fig(fig, 'figures/fig6_lower.pdf')
