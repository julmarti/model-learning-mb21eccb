"""Main functions."""

import numpy as np

from sklearn import linear_model
from scipy.integrate import odeint
from numba import jit
from utils import nmol_conversion, np_max, np_min


@jit(nopython=True)
def clock_model_vitro(y, t, p):
    """
    Circadian clock model.

    variables:
    y = [x1, x2, x5, x6,
         y1, y2, y3, y4, y5,
         z1, z2, z4, z5, z6, z7, z8, z9, y6]

    species:
    y = [CLOCK/BMAL_N, PER/CRY_N, REV-ERB_N, ROR_N,
         Per, Cry, Rev-Erb, Ror, Bmal1,
         CRY_C, PER_C, PER/CRY_C, CLOCK_C, REV-ERB_C, ROR_C,
         BMAL_C, CLOCK/BMAL_C, Clock]

    A description is given in supplementary material.
    """

    # fraction cytoplasm/cell & nucleus/cell from bionumbers mouse hepatocytes
    vc, vn = .93, .07
    # fold activation ratio for Bmal1 known from literature
    foldbmal = 12

    dx1_dt = p[45] * y[16] - p[0] * y[0]
    dx2_dt = p[42] * y[11] - p[1] * y[1]
    dx5_dt = p[43] * y[13] - p[2] * y[2]
    dx6_dt = p[44] * y[14] - p[3] * y[3]

    dy1_dt = p[20] / (1 + (y[1] / p[24])**p[51] * (y[0] / p[23])**p[52] + (y[0] / p[23])**p[52]) * (1 + p[46] * (y[0] / p[23])**p[52]) - p[4] * y[4]
    dy2_dt = p[21] / (1 + (y[1] / p[26])**p[53] * (y[0] / p[25])**p[54] + (y[0] / p[25])**p[54]) * (1 + p[47] * (y[0] / p[25])**p[54]) * (1 / (1 + (y[2] / p[27])**p[55])) - p[5] * y[5]
    dy3_dt = p[56] / (1 + (y[1] / p[29])**p[51] * (y[0] / p[28])**p[52] + (y[0] / p[28])**p[52]) * (1 + p[48] * (y[0] / p[28])**p[52]) - p[6] * y[6]
    dy4_dt = p[57] / (1 + (y[1] / p[31])**p[51] * (y[0] / p[30])**p[52] + (y[0] / p[30])**p[52]) * (1 + p[49] * (y[0] / p[30])**p[52]) - p[7] * y[7]
    dy5_dt = p[22] / (1 + (y[2] / p[33])**p[51] + (y[3] / p[32])**p[52]) * (1 + foldbmal * (y[3] / p[32])**p[52]) - p[8] * y[8]
    dy6_dt = p[58] / (1 + (y[2] / p[35])**p[51] + (y[3] / p[34])**p[52]) * (1 + p[50] * (y[3] / p[34])**p[52]) - p[9] * y[17]

    dz1_dt = p[37] * y[5] + p[19] * y[11] - (p[18] * y[10] + p[10]) * y[9]
    dz2_dt = p[36] * y[4] + p[19] * y[11] - (p[18] * y[9] + p[11]) * y[10]
    dz4_dt = p[18] * y[9] * y[10] - (p[42] * (vc / vn) + p[19]) * y[11]
    dz5_dt = p[41] * y[17] + p[17] * y[16] - (p[16] * y[15] + p[12]) * y[12]
    dz6_dt = p[38] * y[6] - (p[43] * (vc / vn) + p[13]) * y[13]
    dz7_dt = p[39] * y[7] - (p[44] * (vc / vn) + p[14]) * y[14]
    dz8_dt = p[40] * y[8] + p[17] * y[16] - (p[16] * y[12] + p[15]) * y[15]
    dz9_dt = p[16] * y[15] * y[12] - ((vc / vn) * p[45] + p[17]) * y[16]

    dXdt = [dx1_dt, dx2_dt, dx5_dt, dx6_dt,
            dy1_dt, dy2_dt, dy3_dt, dy4_dt, dy5_dt,
            dz1_dt, dz2_dt, dz4_dt, dz5_dt, dz6_dt, dz7_dt,
            dz8_dt, dz9_dt, dy6_dt]
    return dXdt


@jit(nopython=True)
def check_period(Y, t):

    """Period computation heuristic ensuring sustained oscillations.

    Parameters
    ----------

    Y: End of model simulation (e.g. 60 last hours).

    t: time span (e.g. np.linspace(0, 60, 601).

    Returns
    -------

    Boolean for periodic oscillations or not and associated period.

    """

    bool_per, period = 0, 0
    m = np.empty(0)
    for j in range(Y.shape[1]):
        a = np.empty(0)
        b = np.empty(0)
        for i in range(1, len(Y[:, j]) - 1):
            if (Y[i - 1, j] < Y[i, j]) and (Y[i, j] > Y[i + 1, j]):
                a = np.append(a, t[i])
                b = np.append(b, Y[i, j])
        pers = np.diff(a)
        mean = 0 if len(pers) == 0 else np.sum(pers) / len(pers)
        if np.abs(mean - 24) < 6 and b.size != 0:
            mb = np.min(b)
            if mb > 0:
                if np.max(b) / mb < 1.001:
                    m = np.append(m, mean)
                else:
                    return bool_per, period
        else:
            return bool_per, period
    bool_per, period = 1, np.sum(m) / len(m)
    return bool_per, period


@jit(nopython=True)
def check_clock(Y, t):

    """Perform several check of circadian clock wellness.
    Namely, period, relative amplitude and phase differences of key
    components are tested for.

    Parameters
    ----------

    Y: End of model simulation (e.g. 60 last hours).

    t: time span (e.g. np.linspace(0, 60, 601).

    Returns
    -------

    Boolean: accepted/rejected clock.

    """

    bool_check_clock = 0

    # check for amplitude
    amp = (np_max(Y[:240], 0) - np_min(Y[:240], 0)) / np_max(Y[:240], 0)
    if amp.min() < 5e-2:
        return bool_check_clock

    # check for phase differences
    argmax = np.empty(Y.shape[1], dtype=np.int32)
    for i in range(Y.shape[1]):
        argmax[i] = np.argmax(Y[:240, i])
    phase = t[argmax] % 24
    phase_diff_cbpc = np.abs(phase[0] - phase[1])
    phase_diff_rr = np.abs(phase[2] - phase[3])
    if phase_diff_cbpc > 12:
        phase_diff_cbpc = - (phase_diff_cbpc - 24)
    if phase_diff_rr > 12:
        phase_diff_rr = - (phase_diff_rr - 24)
    if phase_diff_rr < 6 or phase_diff_cbpc < 6:
        return bool_check_clock

    # check for period
    bool_per, period = check_period(Y, t)
    if bool_per and np.abs(24 - period) < 4:
        bool_check_clock = 1
    return bool_check_clock


@jit(nopython=True)
def get_transcription(gene_name, Y, p):
    """Gene transcription computation necessary for Eq. (6) / (7)

    Parameters
    ----------

    gene_name: str, gene_name.

    Y: current trace used to generate the learning samples.

    p: clock model parameters.

    Returns
    -------

    Vector of similar size as Y of transcription effect.

    """

    # index of nuclear protein species in the model
    CB, PC, REV, ROR = 0, 1, 2, 3
    if gene_name == 'Per':
        transc = 1 / (1 + (Y[:, PC] / p[24])**p[51] * (Y[:, CB] / p[23])**p[52] + (Y[:, CB] / p[23])**p[52]) * (1 + p[46] * (Y[:, CB] / p[23])**p[52])
    if gene_name == 'Rev-Erb':
        transc = 1 / (1 + (Y[:, PC] / p[29])**p[51] * (Y[:, CB] / p[28])**p[52] + (Y[:, CB] / p[28])**p[52]) * (1 + p[48] * (Y[:, CB] / p[28])**p[52])
    if gene_name == 'Bmal1':
        transc = 1 / (1 + (Y[:, REV] / p[33])**p[51] + (Y[:, ROR] / p[32])**p[52]) * (1 + 12 * (Y[:, ROR] / p[32])**p[52])
    return transc


def get_residual_trajectories(genes, ntraj, hp='transc', seed=1234):
    """Compute residual trajectories for each genes/class
    under hypothesis hp.

    Parameters
    ----------

    genes: np.3D array (class, timepoints, gene), gene expression data

    ntraj: int, number of trajectories to generate per parameter set.

    hp: str, hypothesis under which residual trajectories are computed

    seed: numpy random seed for parameter perturbation

    Returns
    -------

    np.4D array (traj, gene, timepoints, class), residual trajectories.

    """

    np.random.seed(seed)
    param_sets = np.loadtxt('dat/param_sets.txt')
    y0 = np.loadtxt('dat/y0.txt')
    t = np.linspace(0, 600, 6001)
    nclass, npts, ngenes = genes.shape
    names = ['Bmal1', 'Per', 'Rev-Erb']

    # perturb parameter : five from sensibility analysis
    # and those included in equations 6/7 from main text
    if hp == 'transc':
        perturb = [0, 8, 40, 9, 41, 51, 52, 46, 48] + \
                  [23, 24, 28, 29, 32, 33, 4, 6]
        decay_rates = [8, 4, 6]
    else:
        perturb = [0, 22, 40, 9, 41, 51, 52, 46, 48] + \
                  [23, 24, 28, 29, 32, 33, 20, 56]
        Vmax = [22, 20, 56]

    res = np.zeros([0, ngenes, npts - 1, nclass])
    for p in range(len(param_sets)):
        print(f'generating residuals from param set n°{p + 1} for {hp}')
        y = np.zeros([ntraj, ngenes, npts - 1, nclass])
        count = 0
        while count < ntraj:
            coords_perturb = np.random.normal(param_sets[p, perturb],
                                              .1 * param_sets[p, perturb])
            params_perturb = np.copy(param_sets[p])
            params_perturb[perturb] = coords_perturb
            Y = odeint(clock_model_vitro, y0, t, args=(params_perturb,),
                       rtol=10**(-12), atol=10**(-12))
            Y_end = Y[-601:]
            if check_clock(Y_end, t[:601]):
                Y_end *= 10**9
                params_perturb = nmol_conversion(params_perturb)
                check_all_genes = 0
                for g in range(ngenes):
                    # transcription is the same for all classes
                    transc = get_transcription(names[g], Y_end[:240, :4],
                                               params_perturb)
                    if hp != 'transc':
                        transc *= params_perturb[Vmax[g]]
                    for cl in range(nclass):
                        diff = np.diff(genes[cl, :, g]) / .1
                        if hp == 'transc':
                            deg = params_perturb[decay_rates[g]] * genes[cl, :-1, g]
                            y[count, g, :, cl] = (diff + deg) / transc
                        else:
                            y[count, g, :, cl] = (transc - diff) / genes[cl, :-1, g]
                    # positivity of the effect
                    # ensures positive transcription/negative degradation.
                    if y[count, g, :, :].min() > 0:
                        check_all_genes += 1
                if check_all_genes == 3:
                    count += 1
        res = np.concatenate((res, y))
    return res


def linreg(X, y, regsets):

    """Perform linear regression for each trajectory, each
    gene and each class.

    Parameters
    ----------

    X: np.3D array (class, timepoints, reg), regulator data.

    y: np.4D array (traj, gene, timepoints, class), residual trajectories.

    regsets: list of tuples with coordinates between [0, 9].

    Returns
    -------

    w: np.5D array (gene, traj, regset, class, reg), weight vectors

    err: np.4D array (gene, traj, regset, class) mean squared errors

    """

    ntraj, ngenes, npts, nclass, = y.shape
    nbiom = X.shape[2]
    lr = linear_model.LinearRegression(fit_intercept=False)
    w = np.zeros([ngenes, ntraj, len(regsets), nclass, nbiom])
    err = np.zeros([ngenes, ntraj, len(regsets), nclass])
    for j, c in enumerate(regsets):
        for s in range(ntraj):
            for cl in range(nclass):
                lr = lr.fit(X[cl, :, c].T, y[s, :, :, cl].T)
                w[:, s, j, cl, c] = lr.coef_
                for g in range(ngenes):
                    err[g, s, j, cl] = np.sum((y[s, g, :, cl] - np.dot(X[cl, :, c].T, lr.coef_[g]))**2) / npts
    return w, err

