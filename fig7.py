"""Code to generate figure 7."""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from matplotlib.lines import Line2D
from utils import adapt_save_fig, get_graphical_options

plt.rcParams.update({'font.size': 24,
                     'axes.labelsize': 24,
                     'xtick.labelsize': 18,
                     'ytick.labelsize': 18})
options = get_graphical_options()
colors_class, genes = options['colors_class'], options['names_genes'][:-1]
title = options['names_class']
nclass = len(colors_class)
ngenes = len(genes)

data = np.load('dat/best_models_transc_bmalper.npz')
# coefficients of best models for each res traj and each class
bmal_1, bmal_2 = data['bmal_model1'], data['bmal_model2']
per_1, per_2 = data['per_model1'], data['per_model2']

fig, axes = plt.subplots(ngenes, ngenes, figsize=(20, 10))
fig.subplots_adjust(hspace=.25, wspace=.175)
for i in range(nclass):
    sns.kdeplot(bmal_2[:, i, 8], ax=axes[0, 0], color=colors_class[i],
                linewidth=3)
    axes[0, 0].set_xlabel(r'$\int$Food Intake weight')

    sns.kdeplot(bmal_2[:, i, 6], ax=axes[1, 0], color=colors_class[i],
                linewidth=3)
    axes[1, 0].set_xlabel(r'$\int$Temperature weight')

    sns.kdeplot(per_2[:, i, 3], ax=axes[0, 1], color=colors_class[i],
                linewidth=3)
    axes[0, 1].set_xlabel(r'Food Intake weight')

    sns.kdeplot(per_2[:, i, 6], ax=axes[1, 1], color=colors_class[i],
                linewidth=3)
    axes[1, 1].set_xlabel(r'$\int$Temperature weight')

for row in range(ngenes):
    axes[0, row].set_title(r'FI + $\int$T°c $\longrightarrow Per2$ Transcription' if row else r'$\int$FI + $\int$T°c $\longrightarrow Bmal1$ Transcription')
    axes[0, row].set_ylim(0, 2.95)
    axes[1, row].set_ylim(0, 2.95)

# class legend
lines = [Line2D([0], [0], color=c) for c in colors_class]
fig.legend(lines, title,
           bbox_to_anchor=(.85, 1.03), shadow=True,
           ncol=nclass)

adapt_save_fig(fig, 'figures/fig7.pdf')
