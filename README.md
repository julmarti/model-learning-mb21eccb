This repository contains the code used for the article submission 'Model learning to identify systemic regulators ofthe peripheral circadian clock'.

The following Python modules are required:

* Numpy
* Scipy
* Matplotlib
* Seaborn
* Pickle
* Scikit-Learn
* Numba

`Numba` is used to speed computations up. If for some reasons you do not want to use it, simply comment the wrapper line `@jit(nopython=True)` above all function definitions in `algorithm.py` and `utils.py`.

`fig*.py` files will generate all figures present in the main text using precomputed results obtained on 2000 residual trajectories. Just run `chmod 777 generate_figures.sh && ./generate_figures.sh` in a terminal.

The notebook `Example.ipynb` provides a run of the method from scratch, with the generation of new residual trajectories. The number of trajectories to generate can be selected, as well as the seed for the parameter perturbation.

Raw data are contained in the excel file `raw_data.xls`. Both the liver clock gene expression data and the systemic regulators data are present.
