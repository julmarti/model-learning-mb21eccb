"""Code to generate figure 5."""

import numpy as np
import matplotlib.pyplot as plt

from utils import adapt_save_fig, get_graphical_options
from matplotlib.font_manager import FontProperties

plt.rcParams.update({'font.size': 30})

options = get_graphical_options()
genes = options['names_genes']
ngene = len(genes)
colors = options['colors_genes']

# for each gene, for each residual, we compute the loss
# across classes associated with the best-fitting model.
data = np.load('dat/mins.npz')
mins = [data['mins_transc'], data['mins_deg']]

fig, axes = plt.subplots(len(mins), 1, figsize=(20, 14), sharex=True)
plt.subplots_adjust(hspace=.2)
font = FontProperties(style='italic')
n_coeffs = np.arange(1, 6, 1)
space = [-.02, 0, 0.02]
for m in range(len(mins)):
    for g in range(ngene):
        mean = mins[m][g, :, :].mean(axis=0)
        std = mins[m][g, :, :].std(axis=0)

        axes[m].errorbar(n_coeffs + space[g], mean, yerr=std, fmt='.',
                         capsize=6, color=colors[g],
                         markersize=15, elinewidth=3,
                         label=genes[g] if g != 2 else r'$Rev-Erb\alpha$')

    axes[m].set_yticks(np.linspace(0, .6, 5))
    axes[m].grid(linestyle='--')
    axes[m].set_xlim(.9, 5.1)

axes[0].legend(bbox_to_anchor=(1, 1), prop=font)
axes[1].set_xticks(n_coeffs)
axes[1].set_xlabel('Number of active systemic regulators')
fig.text(.04, .34, r'Total error value $\mathcal{E}$', rotation=90)
fig.text(.1, .92, r'$\bf{A}$')
fig.text(.1, .47, r'$\bf{B}$')
adapt_save_fig(fig, 'figures/fig5.pdf')
