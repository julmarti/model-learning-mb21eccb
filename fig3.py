"""Code to generate figure 3."""

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.lines import Line2D
from utils import adapt_save_fig, get_graphical_options

plt.rcParams.update({'font.size': 30})
options = get_graphical_options()
genes = options['names_genes']
colors = options['colors_class']
title = options['names_class']

# contains the mean and std of all residual trajectories
res = np.load('dat/res_traj_meanstd.npz')
transc_mean, transc_std = res['transc_mean'], res['transc_std']
deg_mean, deg_std = res['deg_mean'], res['deg_std']
mean, std = [transc_mean, deg_mean], [transc_std, deg_std]
hp = ['Transcription', 'Degradation']

t = np.linspace(0, 24, 240)
nclass = len(title)
nrows, ncols = len(genes), len(hp)
fig, axes = plt.subplots(nrows, ncols, figsize=(18, 12),
                         sharex=True)
for g in range(nrows):
    for cl in range(nclass):
        for h in range(len(hp)):
            axes[g, h].plot(t, mean[h][g, :, cl],
                            color=colors[cl], linewidth=3)
            axes[g, h].fill_between(t, mean[h][g, :, cl] - std[h][g, :, cl],
                                    mean[h][g, :, cl] + std[h][g, :, cl],
                                    color=colors[cl], alpha=.2)

    for i in range(len(hp)):
        axes[g, i].set_xticks(np.arange(0, 25, 6))
        axes[0, i].set_title(hp[i])
    axes[g, 0].set_ylabel(genes[g], fontstyle='italic')
axes[2, 0].set_ylabel(r'Rev-Erb$\alpha$', fontstyle='italic')
fig.text(.55, 0, 'Hours (ZT)', ha='center', va='center')

# class legend
lines = [Line2D([0], [0], color=c) for c in colors]
fig.legend(lines, title,
           bbox_to_anchor=(.975, 1.06), shadow=True, ncol=4)

fig.tight_layout()
adapt_save_fig(fig, 'figures/fig3.pdf')

