"""Code to generate figure 4."""

import numpy as np
import matplotlib.pyplot as plt

from utils import adapt_save_fig, get_graphical_options
from matplotlib.patches import Patch
from matplotlib.font_manager import FontProperties

plt.rcParams.update({'font.size': 30})
options = get_graphical_options()
colors = options['colors_genes']
genes = options['names_genes']
feats = options['names_biomarkers']
feats = feats + ['$\int$' + f for f in feats]

# Load mean/std absolute Shapley values across residuals/class/models/timepoints
shapval = np.load('dat/shapval_transc_meanstd.npz')
mean, std = shapval['mean'], shapval['std']
ngenes, nfeats = len(genes), len(feats)
space = [-0.25, 0, 0.25]

fig = plt.figure(figsize=(25, 8))
patches = [Patch(fill=True, color=colors[i]) for i in range(ngenes)]
font = FontProperties(style='italic')
for g in range(ngenes):
    for l in range(nfeats):
        plt.bar(l + space[g], mean[g, l], yerr=std[g, l],
                color=colors[g], width=.25, capsize=4)
        plt.xticks(np.arange(10), feats, rotation=90)
    plt.ylabel('Mean absolute Shapley values')
fig.legend(patches, [genes[k] for k in range(2)] + [r'$Rev-Erb\alpha$'],
           bbox_to_anchor=(.875, .875), prop=font)
adapt_save_fig(fig, 'figures/fig4.pdf')
