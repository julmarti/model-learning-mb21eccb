import numpy as np

from numba import jit


def get_graphical_options():

    """Global definition of some shared properties across figures."""

    options = {}
    options['colors_biomarkers'] = ['springGreen', 'crimson'] + \
                                   ['sandybrown', 'darkorchid', 'darkcyan']
    options['colors_genes'] = ['darkblue', 'orange', 'green']
    options['colors_class'] = ['orchid', 'cornflowerblue', 'darkred', 'blue']
    options['names_class'] = ['Class 1 (♀)', 'Class 2 (♂)'] + \
                             ['Class 3 (♀)', 'Class 4 (♂)']
    options['names_genes'] = ['Bmal1', 'Per2', 'Rev-Erb']
    options['names_biomarkers'] = ['Activity', 'Temperature'] + \
                                  ['Corticosterone', 'Food Intake'] +\
                                  ['Melatonin']
    options['names_genes_hepato'] = ['Per2', 'Cry1', 'Rev-ErbA', 'RorC'] + \
                                    ['Bmal1', 'Clock']
    return options


def adapt_save_fig(fig, filename='test.pdf'):

    """Remove right and top spines, set bbox_inches and dpi."""

    for ax in fig.get_axes():
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
    fig.savefig(filename, bbox_inches='tight', dpi=300)


def nmol_conversion(params):

    """Scaling the simulated model from Mol/L to nmol/L."""

    scaled_params = np.copy(params)
    # all Vmax, kt and ki params
    list_upscaling = [i for i in range(20, 36)] + [56, 57, 58]
    # kfz4, kfz9 complexation rates
    list_downscaling = [18, 16]
    for i in list_upscaling:
        scaled_params[i] *= 10**9
    for i in list_downscaling:
        scaled_params[i] /= 10**9
    return scaled_params


# functions to use numpy reduction functions with numba

@jit(nopython=True)
def np_apply_along_axis(func1d, axis, arr):
    if axis == 0:
        result = np.empty(arr.shape[1])
        for i in range(len(result)):
            result[i] = func1d(arr[:, i])
    else:
        result = np.empty(arr.shape[0])
        for i in range(len(result)):
            result[i] = func1d(arr[i, :])
    return result


@jit(nopython=True)
def np_max(array, axis):
    return np_apply_along_axis(np.max, axis, array)


@jit(nopython=True)
def np_min(array, axis):
    return np_apply_along_axis(np.min, axis, array)

