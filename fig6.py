"""Code to generate figure 6."""

import numpy as np
import matplotlib.pyplot as plt

from utils import adapt_save_fig, get_graphical_options
from matplotlib.lines import Line2D

plt.rcParams.update({'font.size': 30})

data = np.load('dat/scores_transc_2terms.npz')
mean, std, coeffs = data['mean'], data['std'], data['coeffs']
options = get_graphical_options()
bio = options['names_biomarkers']
col_bio = options['colors_biomarkers']
genes = options['names_genes']
ngenes = len(genes)
feats = bio + ['$\int$' + f for f in bio]
nfeats = len(feats)

# initialize dict for mean ranks computation
ranks = {}
for n in genes:
    ranks[n] = {}
    for f in feats:
        ranks[n][f] = []

heights = [6, 1]
widths = [2, 2, 1]
fig = plt.figure(figsize=(25, 11), constrained_layout=True)
gs = fig.add_gridspec(2, 3, height_ratios=heights, width_ratios=widths)
fig_ax1 = fig.add_subplot(gs[0, :])
mean_ranks = np.zeros([ngenes, 10])
for g in range(ngenes):
    sort = np.argsort(mean[g])
    for l, k in enumerate(sort):
        idx = np.argsort(coeffs[g, k])[::-1][:2]
        eb = fig_ax1.errorbar(l * 1.25 + g * 50, mean[g, k], std[g, k],
                              fmt='none', capsize=4, color='black', elinewidth=.25)
        eb[-1][0].set_linestyle('-.')

        fig_ax1.plot(l * 1.25 + g * 50, mean[g, k] - .01, markersize=10, marker='s',
                     c=col_bio[idx[1] % 5],
                     markerfacecolor=col_bio[idx[1] % 5] if idx[1] < 5 else 'none')

        fig_ax1.plot(l * 1.25 + g * 50, mean[g, k] + .02, markersize=10, marker='s',
                     c=col_bio[idx[0] % 5],
                     markerfacecolor=col_bio[idx[0] % 5] if idx[0] < 5 else 'none')

        ranks[genes[g]][feats[idx[0]]].append(l)
        ranks[genes[g]][feats[idx[1]]].append(l)

    fig.text(.15 + .32 * g, .925,
             genes[g] if g != 2 else r'$Rev-Erb\alpha$', fontstyle='italic')

    for f, r in enumerate(ranks[genes[g]].values()):
        mean_ranks[g, f] = np.mean(r)
plt.setp(fig_ax1.get_xticklabels(), visible=False)
fig_ax1.set_ylabel(r'Total error value $\mathcal{E}$')
fig_ax1.set_ylim(-.1, 1)
fig_ax1.set_xlim(-1, 150)
fig_ax1.axhspan(.15, 1, facecolor='0.2', alpha=0.45)
fig_ax1.axhspan(.10, .15, facecolor='0.2', alpha=0.3)
fig_ax1.axhspan(.05, .1, facecolor='0.2', alpha=0.1)
fig.text(0, .99, r'$\bf{A}$')
fig.text(0, .43, r'$\bf{B}$')
fig.text(.415, .43, r'$\bf{C}$')
fig_ax1.set_xlabel('Models sorted from best to worse for each gene')

fig_ax2 = fig.add_subplot(gs[1, 0])
fig_ax3 = fig.add_subplot(gs[1, 1])
figg = [fig_ax2, fig_ax3]

for g in range(ngenes - 1):
    idx_sorted = np.argsort(mean_ranks[g])
    for l in range(len(idx_sorted)):
        figg[g].bar(l, mean_ranks[g][idx_sorted[l]],
                    color=col_bio[idx_sorted[l] % 5], width=.4)
    figg[g].set_xticks(np.arange(len(idx_sorted)))
    figg[g].set_xticklabels([feats[p] for p in idx_sorted], rotation=90)
    figg[g].set_title(genes[g], fontstyle='italic')
    figg[g].set_yticks(np.arange(0, 31, 15))

lines = [Line2D([0], [0], color='w', markerfacecolor=c,
         marker='s', markersize=15) for c in col_bio] + \
        [Line2D([0], [0], color='w', markerfacecolor='black',
         marker='s', markersize=15)] + \
        [Line2D([0], [0], color='w', markeredgecolor='black',
         marker='s', markersize=15, linestyle='--')]

fig.text(0, .22, 'Mean rank', rotation=90)
labels = ['Biomarker', '$\int$ Biomarker']
fig.legend(lines, bio + labels,
           bbox_to_anchor=(1.01, .48), shadow=True, ncol=1)
adapt_save_fig(fig, 'figures/fig6.pdf')
