"""Code to generate figure 2."""

import numpy as np
import matplotlib.pyplot as plt

from scipy.integrate import odeint
from matplotlib.ticker import StrMethodFormatter
from utils import adapt_save_fig, get_graphical_options
from algorithm import clock_model_vitro

data = np.loadtxt('dat/invitro_hepato_microarray_mrna_data.txt')
genes = get_graphical_options()['names_genes_hepato']
y0 = np.loadtxt('dat/y0.txt')
params = np.loadtxt('dat/param_sets.txt')[0]

t = np.linspace(0, 600, 6001)
Y = odeint(clock_model_vitro, y0, t, args=(params,),
           rtol=10**(-12), atol=10**(-12))
# keep only end for stable behavior and convert to nmol/L
Y_end = Y[-601:] * 10**9

# time points for data
time = np.arange(0, 48, 2)
# time points at which model is compared to data
t_interp = np.array(time * 10, dtype=np.int32)
alpha = np.ones(6)
# scaling factor for Cry Ror Clock for which
# we do not have absolute mean concentrations
alpha[[1, 3, 5]] = (Y_end[:, [5, 7, 17]][t_interp] / data[:, [1, 3, 5]]).mean(0)

plt.rcParams['xtick.labelsize'] = 28
plt.rcParams['ytick.labelsize'] = 28

fig, axes = plt.subplots(2, 3, figsize=(25, 11))
fig.text(0, .5, 'Scaled gene expression (nmol/L)',
         ha='center', va='center', rotation='vertical', fontsize=38)
fig.text(.5, 0, 'Circadian Time (h)', ha='center', va='center', fontsize=38)
fig.tight_layout(pad=2)

genes_idx_model = [4, 5, 6, 7, 8, 17]
for (i, k), ax in zip(enumerate(genes_idx_model), axes.flat):
    ax.scatter(time, alpha[i] * data[:, i],
               marker='o', color='darkblue', s=50)
    ax.plot(t[:601], Y_end[:, k], color='red', linewidth=4)
    ax.set_title(genes[i] if i != 2 else r'$Rev-Erb\alpha$',
                 fontsize=35, fontstyle='italic')

    ax.set_xticks(np.arange(0, 61, 12))
    ymin, ymax = ax.get_ylim()
    ax.set_yticks(np.array([ymin + (.01 * ymin),
                            (ymin + ymax) / 2, ymax - (.01 * ymax)]))
    ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.2f}'))
axes[1, 0].set_title(r'$Ror\gamma$', fontsize=35)
adapt_save_fig(fig, 'figures/fig2.pdf')
