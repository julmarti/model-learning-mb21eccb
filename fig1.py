"""Code to generate figure 1."""

import numpy as np
import matplotlib.pyplot as plt
import pickle as pkl

from utils import adapt_save_fig, get_graphical_options

plt.rcParams.update({'font.size': 25})
options = get_graphical_options()
bio = options['names_biomarkers']
col_bio = options['colors_biomarkers']
title = options['names_class']

# load the data:
with open('dat/circ_biom.dat', 'rb') as f:
    data = pkl.load(f)

time_gp_24 = np.linspace(0, 24, 240)
time_gp_72 = np.linspace(0, 72, 720)

nclass, nrows = 4, 3
fig, axes = plt.subplots(nrows, nclass, figsize=(20, 10),
                         sharex='row', sharey='row')
for i in range(nclass):
    axes[0, i].errorbar(data['temperature']['time'],
                        data['temperature']['mean'][i],
                        data['temperature']['std'][i],
                        fmt='.', color=col_bio[1],
                        markersize=3, errorevery=15)
    axes[0, i].set_xticks(np.arange(0, 73, 18))
    axes[0, i].set_yticks(np.array([36, 37.5, 39]))
    axes[0, i].plot(time_gp_72, data['temperature']['gp_mean'][i],
                    color=col_bio[1], linewidth=3)
    axes[0, i].set_title(title[i])

    # right y-axis
    ax2 = axes[0, i].twinx()
    ax2.errorbar(data['activity']['time'],
                 data['activity']['mean'][i],
                 data['activity']['std'][i],
                 fmt='.', color=col_bio[0],
                 markersize=3, errorevery=15)
    ax2.plot(time_gp_72, data['activity']['gp_mean'][i],
             color=col_bio[0])
    ax2.set_yticks(np.arange(0, 81, 40))
    ax2.set_ylim(-5, 81)
    # row sharey for second y-axis
    if i != 3:
        ax2.yaxis.set_ticklabels([])
        ax2.yaxis.set_ticks([])

    axes[1, i].errorbar(data['cortisol']['time'],
                        data['cortisol']['mean'][i],
                        data['cortisol']['std'][i],
                        fmt='.', color=col_bio[2])
    axes[1, i].plot(time_gp_24, data['cortisol']['gp_mean'][i],
                    color=col_bio[2], linewidth=3)
    axes[1, i].set_yticks(np.arange(0, 175, 80))

    # right y-axis
    ax2 = axes[1, i].twinx()
    ax2.errorbar(data['melatonin']['time'],
                 data['melatonin']['mean'][i],
                 data['melatonin']['std'][i],
                 fmt='.', color=col_bio[4])
    ax2.plot(time_gp_24, data['melatonin']['gp_mean'][i],
             color=col_bio[4], linewidth=3)
    ax2.set_xticks(np.arange(0, 25, 6))
    ax2.set_yticks(np.arange(0, 50, 20))
    # row sharey for second y-axis
    if i != 3:
        ax2.yaxis.set_ticklabels([])
        ax2.yaxis.set_ticks([])

    axes[2, i].errorbar(data['food_nutrients']['time'],
                        data['food_nutrients']['mean'][i],
                        data['food_nutrients']['std'][i],
                        fmt='.', color=col_bio[3])
    axes[2, i].plot(time_gp_24, data['food_nutrients']['gp_mean'][i],
                    color=col_bio[3], linewidth=3)
    axes[2, i].set_xticks(np.arange(0, 25, 6))
    axes[2, i].set_yticks(np.arange(0, 6, 2))

axes[0, 0].set_ylabel(f'{bio[1]}\n(°c)', color=col_bio[1])
axes[1, 0].set_ylabel(f'{bio[2]}\n(ng/mL)', color=col_bio[2])
axes[2, 0].set_ylabel(f'{bio[3]}\n(g)', color=col_bio[3])
fig.text(.95, .725, f'{bio[0]}\n(m.s$^{-2}$)',
         rotation=270, ha='center', color=col_bio[0])
fig.text(.95, .425, f'{bio[4]}\n(ng/mL)',
         rotation=270, ha='center', color=col_bio[4])
fig.text(.275, .94, 'B6D2F1')
fig.text(.675, .94, 'B6CBAF1')
fig.text(.47, 0.02, 'Hours (ZT)')
adapt_save_fig(fig, 'figures/fig1.pdf')
